@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6 form-group"><a href="{{route('product.create')}}" class="btn btn-success mr-2">Create
                Product</a>
        </div>
        <div class="col-md-6 form-group">
            <form action="{{route('product.index')}}" method="get">
                @csrf
                <input type="text" class="form-control" placeholder="search...." name="name" value="{{ request()->query('name') }}">
            </form>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Address</th>
            <th scope="col">Amallar</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>{{$product->name}}</td>
                <td>{{$product->address}}</td>
                <td>
                    <a href="{{route('product.edit',['product'=>$product->id])}}"><i class="bi bi-pen"></i></a>
                    <a href="{{route('product.show',['product'=>$product->id])}}"><i class="bi bi-eye"></i></a>
                    <button class="btn btn-danger" form="delete-form-{{$product->id}}"><i
                            class="bi bi-trash"></i></button>
                    <form id="delete-form-{{$product->id}}"
                          action="{{route('product.destroy',['product'=>$product->id])}}" method="post">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $products->links() }}
@endsection
