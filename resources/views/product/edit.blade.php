@extends('layouts.app')
@section('content')
    <form action="{{route('product.update',['product'=>$product->id])}}" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Name</label>
            <input type="text" class="form-control" name="name" value="{{$product->name}}">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Phone</label>
            <input type="text" class="form-control" name="address" value="{{$product->address}}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
