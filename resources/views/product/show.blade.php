@extends('layouts.app')
@section('content')
    <table class="table">
        <tr>
            <td><strong>Name</strong></td>
            <td>{{ $product->name }}</td>
        </tr>
        <tr>
            <td><strong>Address</strong></td>
            <td>{{ $product->address }}</td>
        </tr>
    </table>

    <a href="{{ back()->getTargetUrl() }}" class="btn btn-primary">back</a>
@endsection
